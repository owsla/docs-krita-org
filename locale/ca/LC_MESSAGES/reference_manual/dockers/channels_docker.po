# Translation of docs_krita_org_reference_manual___dockers___channels_docker.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-18 14:16+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/dockers/channels_docker.rst:1
msgid "Overview of the channels docker."
msgstr "Vista general de l'acoblador Canals."

#: ../../reference_manual/dockers/channels_docker.rst:10
msgid "Color"
msgstr "Color"

#: ../../reference_manual/dockers/channels_docker.rst:10
msgid "Color Channels"
msgstr "Canals de color"

#: ../../reference_manual/dockers/channels_docker.rst:15
msgid "Channels"
msgstr "Canals"

#: ../../reference_manual/dockers/channels_docker.rst:18
msgid ".. image:: images/dockers/Krita_Channels_Docker.png"
msgstr ".. image:: images/dockers/Krita_Channels_Docker.png"

#: ../../reference_manual/dockers/channels_docker.rst:19
msgid ""
"The channel docker allows you to turn on and off the channels associated "
"with the color space that you are using. Each channel has an enabled and "
"disabled checkbox. You cannot edit individual layer channels from this "
"docker."
msgstr ""
"L'acoblador Canals permet activar i desactivar els canals associats amb "
"l'espai de color que utilitzeu. Cada canal té una casella de selecció "
"habilitada i inhabilitada. No podreu editar canals de capa individuals des "
"d'aquest acoblador."

#: ../../reference_manual/dockers/channels_docker.rst:22
msgid "Editing Channels"
msgstr "Editar els canals"

#: ../../reference_manual/dockers/channels_docker.rst:24
msgid ""
"If you want to edit individual channels by their grayscale component, you "
"will need to manually separate a layer. This can be done with a series of "
"commands with the layer docker."
msgstr ""
"Si voleu editar canals individuals pel seu component d'escala de grisos, "
"haureu de separar una capa manualment. Això es pot fer amb una sèrie "
"d'ordres amb l'acoblador Capes."

#: ../../reference_manual/dockers/channels_docker.rst:26
msgid "Select the layer you want to break apart."
msgstr "Seleccioneu la capa que voleu separar."

#: ../../reference_manual/dockers/channels_docker.rst:27
msgid "Go to :menuselection:`Image --> Separate Image`"
msgstr "Aneu a :menuselection:`Imatge --> Descompon la imatge`."

#: ../../reference_manual/dockers/channels_docker.rst:28
msgid "Select the following options and click :guilabel:`OK`:"
msgstr "Seleccioneu les següents opcions i feu clic a :guilabel:`D'acrod`:"

#: ../../reference_manual/dockers/channels_docker.rst:30
msgid "Source: Current Layer"
msgstr "Origen: Capa actual."

#: ../../reference_manual/dockers/channels_docker.rst:31
msgid "Alpha Options: Create separate separation from alpha channel"
msgstr ""
"Opcions de l'alfa: Crea una descomposició separada a partir del canal alfa."

#: ../../reference_manual/dockers/channels_docker.rst:32
msgid "Output to Grayscale, not color: unchecked"
msgstr "Sortida en escala de grisos, sense color: sense marcar."

#: ../../reference_manual/dockers/channels_docker.rst:34
msgid "Hide your original layer"
msgstr "Oculteu la capa original."

# skip-rule: t-acc_obe
#: ../../reference_manual/dockers/channels_docker.rst:35
msgid ""
"Select All of the new channel layers and put them in a group layer (:"
"menuselection:`Layer --> Quick Group`)"
msgstr ""
"Seleccioneu Totes les capes de canal noves i col·loqueu-les en una capa de "
"grup (element de menú :menuselection:`Capa --> Agrupa ràpid`)."

#: ../../reference_manual/dockers/channels_docker.rst:36
msgid ""
"Select the Red layer and change the blending mode to \"Copy Red\" (these are "
"in the Misc. category)"
msgstr ""
"Seleccioneu la capa Vermell i canvieu el mode de barreja a «Copia el "
"vermell» (es troben a la categoria Miscel·lània)."

#: ../../reference_manual/dockers/channels_docker.rst:37
msgid "Select the Green layer and change the blending mode  to \"Copy Green\""
msgstr ""
"Seleccioneu la capa Verd i canvieu el mode de barreja a «Copia el verd»."

#: ../../reference_manual/dockers/channels_docker.rst:38
msgid "Select the Blue layer and change the blending mode to \"Copy Blue\""
msgstr ""
"Seleccioneu la capa Blau i canvieu el mode de barreja a «Copia el blau»."

#: ../../reference_manual/dockers/channels_docker.rst:39
msgid "Make sure the Alpha layer is at the bottom of the group."
msgstr "Assegureu-vos que la capa Alfa es troba a la part inferior del grup."

#: ../../reference_manual/dockers/channels_docker.rst:40
msgid "Enable Inherit Alpha for the Red, Green, and Blue layers."
msgstr "Habiliteu Hereta l'alfa per a les capes Vermell, Verd i Blau."

# skip-rule: t-acc_obe
#: ../../reference_manual/dockers/channels_docker.rst:42
msgid ""
"Here is a `video to see this process <https://www.youtube.com/watch?"
"v=lWuwegJ-mIQ&feature=youtu.be>`_ in Krita 3.0."
msgstr ""
"Aquí teniu un `vídeo per a veure aquest procés <https://www.youtube.com/"
"watch?v=lWuwegJ-mIQ&feature=youtu.be>`_ en el Krita 3.0."

#: ../../reference_manual/dockers/channels_docker.rst:44
msgid ""
"When working with editing channels, it can be easier to use the Isolate "
"Layer feature to only see the channel. Right-click on the layer to find "
"Isolate Layer."
msgstr ""
"Quan es treballa amb l'edició dels canals, pot ser més fàcil utilitzar la "
"característica Aïlla la capa per a només veure els canals. Feu clic dret "
"sobre la capa per trobar aquesta opció."
