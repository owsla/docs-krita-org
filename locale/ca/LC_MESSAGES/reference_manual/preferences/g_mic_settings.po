# Translation of docs_krita_org_reference_manual___preferences___g_mic_settings.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 16:11+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/preferences/g_mic_settings.rst:1
msgid "How to setup G'Mic in Krita."
msgstr "Com configurar el G'Mic al Krita."

#: ../../reference_manual/preferences/g_mic_settings.rst:10
msgid "Preferences"
msgstr "Preferències"

#: ../../reference_manual/preferences/g_mic_settings.rst:10
msgid "Settings"
msgstr "Ajustaments"

#: ../../reference_manual/preferences/g_mic_settings.rst:10
msgid "Filters"
msgstr "Filtres"

#: ../../reference_manual/preferences/g_mic_settings.rst:10
msgid "G'Mic"
msgstr "G'Mic"

#: ../../reference_manual/preferences/g_mic_settings.rst:15
msgid "G'Mic Settings"
msgstr "Ajustaments per al G'Mic"

# skip-rule: t-sigl_sing2
#: ../../reference_manual/preferences/g_mic_settings.rst:17
msgid ""
"G'Mic or GREYC's Magic for Image Computing is an opensource filter "
"framework, or, it is an extra program you can download to have access to a "
"whole lot of image filters."
msgstr ""
"G'Mic (o GREYC's Magic for Image Computing) és un marc de treball per a "
"filtre de codi obert, o és un programa addicional que podreu descarregar per "
"tenir accés a una gran quantitat de filtres d'imatge."

#: ../../reference_manual/preferences/g_mic_settings.rst:19
msgid ""
"Krita has had G'Mic integration for a long time, but this is its most stable "
"incarnation."
msgstr ""
"El Krita ha tingut una integració amb el G'Mic durant molt de temps, però "
"aquesta és la seva encarnació més estable."

#: ../../reference_manual/preferences/g_mic_settings.rst:21
msgid "You set it up as following:"
msgstr "Configureu-lo de la següent manera:"

# skip-rule: t-acc_obe
#: ../../reference_manual/preferences/g_mic_settings.rst:23
msgid ""
"First download the proper krita plugin from `the G'Mic website. <https://"
"gmic.eu/download.shtml>`_."
msgstr ""
"Primer descarregueu el connector adequat per al Krita des del `lloc web de "
"G'Mic <https://gmic.eu/download.shtml>`_."

#: ../../reference_manual/preferences/g_mic_settings.rst:24
msgid "Then, unzip and place it somewhere you can find it."
msgstr ""
"A continuació, descomprimiu el ZIP i col·loqueu-lo en algun lloc on es pugui "
"trobar."

#: ../../reference_manual/preferences/g_mic_settings.rst:25
msgid ""
"Go to :menuselection:`Settings --> Configure Krita --> G'Mic plugin` and set "
"G'MIC to the filepath there."
msgstr ""
"Aneu a :menuselection:`Arranjament --> Configura el Krita --> Connector "
"G'Mic` i establiu aquí el camí dels fitxers de G'Mic."

#: ../../reference_manual/preferences/g_mic_settings.rst:26
msgid "Then restart Krita."
msgstr "Després reinicieu el Krita."

#: ../../reference_manual/preferences/g_mic_settings.rst:30
msgid "Updates to G'Mic"
msgstr "Actualitzacions de G'Mic"

#: ../../reference_manual/preferences/g_mic_settings.rst:32
msgid ""
"There is a refresh button at the bottom of the G'Mic window that will update "
"your version. You will need an internet connection to download the latest "
"version."
msgstr ""
"Hi ha un botó d'actualització a la part inferior de la finestra de G'Mic que "
"actualitzarà la seva versió. Necessitareu una connexió a Internet per a "
"descarregar l'última versió."

#: ../../reference_manual/preferences/g_mic_settings.rst:34
msgid ""
"If you have issues downloading the update through the plugin, you can also "
"do it manually. If you are trying to update and get an error, copy the URL "
"that is displayed in the error dialog. It will be to a \".gmic\" file. "
"Download it from from your web browser and place the file in one of the "
"following directories."
msgstr ""
"Si teniu problemes per a descarregar l'actualització a través del connector, "
"també podreu fer-ho manualment. Si esteu intentant actualitzar i obteniu un "
"error, copieu l'URL que es mostra en el diàleg d'error. Serà a un fitxer «."
"gmic». Descarregueu-lo des del vostre navegador web i poseu-lo en un dels "
"següents directoris."

#: ../../reference_manual/preferences/g_mic_settings.rst:36
msgid "Windows : %APPDATA%/gmic/update2XX.gmic"
msgstr "Windows: %APPDATA%/gmic/update2XX.gmic"

#: ../../reference_manual/preferences/g_mic_settings.rst:37
msgid "Linux : $HOME/.config/gmic/update2XX.gmic"
msgstr "Linux: $HOME/.config/gmic/update2XX.gmic"

#: ../../reference_manual/preferences/g_mic_settings.rst:39
msgid ""
"Load up the G'Mic plugin and press the refresh button for the version to "
"update."
msgstr ""
"Carregueu el connector G'Mic i premeu el botó Refresca per actualitzar la "
"versió."
