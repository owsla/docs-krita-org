# Translation of docs_krita_org_reference_manual___tools___pan.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 16:16+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: clic del mig del ratolí"

#: ../../<rst_epilog>:80
msgid ""
".. image:: images/icons/pan_tool.svg\n"
"   :alt: toolpan"
msgstr ""
".. image:: images/icons/pan_tool.svg\n"
"   :alt: eina de desplaçar"

#: ../../reference_manual/tools/pan.rst:1
msgid "Krita's pan tool reference."
msgstr "Referència de l'eina Desplaçar del Krita."

#: ../../reference_manual/tools/pan.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/pan.rst:11
msgid "Pan"
msgstr "Desplaçar"

#: ../../reference_manual/tools/pan.rst:16
msgid "Pan Tool"
msgstr "Eina de desplaçar"

#: ../../reference_manual/tools/pan.rst:18
msgid "|toolpan|"
msgstr "|toolpan|"

#: ../../reference_manual/tools/pan.rst:20
msgid ""
"The pan tool allows you to pan your canvas around freely. It can be found at "
"the bottom of the toolbox, and you just it by selecting the tool, and doing |"
"mouseleft| :kbd:`+ drag` over the canvas."
msgstr ""
"L'eina de desplaçar permet desplaçar-vos lliurement pel llenç. Es trobarà a "
"la part inferior del quadre d'eines, i només haureu de seleccionar l'eina i :"
"kbd:`fer` |mouseleft| :kbd:`+ arrossega` sobre el llenç."

#: ../../reference_manual/tools/pan.rst:22
msgid ""
"There are two hotkeys associated with this tool, which makes it easier to "
"access from the other tools:"
msgstr ""
"Hi ha dues dreceres associades amb aquesta eina, les quals faciliten l'accés "
"des de les altres eines:"

#: ../../reference_manual/tools/pan.rst:24
msgid ":kbd:`Space +` |mouseleft| :kbd:`+ drag` over the canvas."
msgstr ":kbd:`Espai + fer` |mouseleft| :kbd:`+ arrossega` sobre el llenç."

#: ../../reference_manual/tools/pan.rst:25
msgid "|mousemiddle| :kbd:`+ drag` over the canvas."
msgstr ":kbd:`Fer` |mousemiddle| :kbd:`+ arrossega` sobre el llenç."

#: ../../reference_manual/tools/pan.rst:27
msgid "For more information on such hotkeys, check :ref:`navigation`."
msgstr ""
"Per obtenir més informació sobre aquestes dreceres, consulteu la :ref:"
"`navigation` al manual d'usuari."
