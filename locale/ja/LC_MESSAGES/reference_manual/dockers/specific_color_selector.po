msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/dockers/specific_color_selector.rst:1
msgid "Overview of the specific color selector docker."
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:11
#: ../../reference_manual/dockers/specific_color_selector.rst:16
msgid "Specific Color Selector"
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:11
msgid "Color"
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:11
msgid "Color Selector"
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:11
msgid "Color Space"
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:19
msgid ".. image:: images/dockers/Krita_Specific_Color_Selector_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:20
msgid ""
"The specific color selector allows you to choose specific colors within a "
"color space."
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:23
msgid "Color Space Chooser"
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:25
msgid ""
"Fairly straightforward. This color space chooser allows you to pick the "
"color space, the bit depth and the icc profile in which you are going to "
"pick your color. Use the checkbox 'show color space selector' to hide this "
"feature."
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:29
msgid "Sliders"
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:31
msgid ""
"These change per color space. If you chose 16bit float or 32 bit float, "
"these will go from 0 to 1.0, with the decimals deciding the difference "
"between colors."
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:35
msgid "Hex Color Selector"
msgstr ""

#: ../../reference_manual/dockers/specific_color_selector.rst:37
msgid ""
"This is only available for the color spaces with a depth of 8 bit. This "
"allows you to input hex color codes, and receive the RGB, CMYK, LAB, XYZ or "
"YCrCb equivalent, and the other way around!"
msgstr ""
