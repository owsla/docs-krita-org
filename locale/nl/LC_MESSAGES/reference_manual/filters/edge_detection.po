# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-05-06 11:29+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "XYZ"
msgstr "XYZ"

#: ../../reference_manual/filters/edge_detection.rst:None
msgid ".. image:: images/filters/Krita_4_0_height_to_normal_map.png"
msgstr ".. image:: images/filters/Krita_4_0_height_to_normal_map.png"

#: ../../reference_manual/filters/edge_detection.rst:1
msgid "Overview of the edge detection filters."
msgstr "Overzicht van de randdetectiefilters."

#: ../../reference_manual/filters/edge_detection.rst:11
#: ../../reference_manual/filters/edge_detection.rst:16
#: ../../reference_manual/filters/edge_detection.rst:23
msgid "Edge Detection"
msgstr "Randdetectie"

#: ../../reference_manual/filters/edge_detection.rst:11
#: ../../reference_manual/filters/edge_detection.rst:40
#: ../../reference_manual/filters/edge_detection.rst:83
msgid "Prewitt"
msgstr "Prewitt"

#: ../../reference_manual/filters/edge_detection.rst:11
#: ../../reference_manual/filters/edge_detection.rst:43
#: ../../reference_manual/filters/edge_detection.rst:86
msgid "Sobel"
msgstr "Sobel"

#: ../../reference_manual/filters/edge_detection.rst:18
msgid ""
"Edge detection filters focus on finding sharp contrast or border between "
"colors in an image to create edges or lines."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:20
msgid "Since 4.0 there are only two edge detection filters."
msgstr "Sinds 4.0 zijn er slechts twee randdetectiefilters."

#: ../../reference_manual/filters/edge_detection.rst:27
msgid ""
"A general edge detection filter that encapsulates all other filters. Edge "
"detection filters that were separate before 4.0 have been folded into this "
"one. It is also available for filter layers and filter brushes."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:33
msgid ".. image:: images/filters/Krita_4_0_edge_detection.png"
msgstr ".. image:: images/filters/Krita_4_0_edge_detection.png"

#: ../../reference_manual/filters/edge_detection.rst:33
msgid ""
"From left to right: Original, with Prewitt edge detection applied, with "
"Prewitt edge detection applied and result applied to alpha channel, and "
"finally the original with an edge detection filter layer with the same "
"settings as 3, and the filter layer blending mode set to multiply"
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:36
#: ../../reference_manual/filters/edge_detection.rst:79
msgid ""
"The convolution kernel formula for the edge detection. The difference "
"between these is subtle, but still worth experimenting with."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:38
#: ../../reference_manual/filters/edge_detection.rst:81
msgid "Simple"
msgstr "Eenvoudig"

#: ../../reference_manual/filters/edge_detection.rst:39
#: ../../reference_manual/filters/edge_detection.rst:82
msgid ""
"A Kernel that is not square unlike the other two, and while this makes it "
"fast, it doesn't take diagonal pixels into account."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:41
#: ../../reference_manual/filters/edge_detection.rst:84
msgid ""
"A square kernel that includes the diagonal pixels just as strongly as the "
"orthogonal pixels. Gives a very strong effect."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:43
#: ../../reference_manual/filters/edge_detection.rst:86
msgid "Formula"
msgstr "Formule"

#: ../../reference_manual/filters/edge_detection.rst:43
#: ../../reference_manual/filters/edge_detection.rst:86
msgid ""
"A square kernel that includes the diagonal pixels slightly less strong than "
"the orthogonal pixels. Gives a more subtle effect than Prewitt."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:46
msgid "The output."
msgstr "De uitvoer."

#: ../../reference_manual/filters/edge_detection.rst:48
msgid "All sides"
msgstr "Alle kanten"

#: ../../reference_manual/filters/edge_detection.rst:49
msgid ""
"Convolves the edge detection into all directions and combines the result "
"with the Pythagorean theorem. This will be good for most uses."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:50
msgid "Top Edge"
msgstr "Bovenrand"

#: ../../reference_manual/filters/edge_detection.rst:51
msgid ""
"This only detects changes going from top to bottom and thus only has top "
"lines."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:52
msgid "Bottom Edge"
msgstr "Onderrand"

#: ../../reference_manual/filters/edge_detection.rst:53
msgid ""
"This only detects changes going from bottom to top and thus only has bottom "
"lines."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:54
msgid "Right Edge"
msgstr "Rechter rand"

#: ../../reference_manual/filters/edge_detection.rst:55
msgid ""
"This only detects changes going from right to left and thus only has right "
"lines."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:56
msgid "Left Edge"
msgstr "Linker rand"

#: ../../reference_manual/filters/edge_detection.rst:57
msgid ""
"This only detects changes going from left to right and thus only has left "
"lines."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:59
msgid "Output"
msgstr "Uitvoer"

#: ../../reference_manual/filters/edge_detection.rst:59
msgid "Direction in Radians"
msgstr "Richting in radialen"

#: ../../reference_manual/filters/edge_detection.rst:59
msgid ""
"This convolves into all directions and then tries to output the direction of "
"the line in radians."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:61
#: ../../reference_manual/filters/edge_detection.rst:90
msgid "Horizontal/Vertical radius"
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:62
msgid ""
"The radius of the edge detection. Default is 1 and going higher will "
"increase the thickness of the lines."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:64
msgid "Apply result to Alpha Channel."
msgstr "Resultaat toepassen op het alfakanaal."

#: ../../reference_manual/filters/edge_detection.rst:64
msgid ""
"The edge detection will be used on a grayscale copy of the image, and the "
"output will be onto the alpha channel of the image, meaning it will output "
"lines only."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:66
msgid "Height Map"
msgstr "Hoogte kaart"

#: ../../reference_manual/filters/edge_detection.rst:66
msgid "Normal Map"
msgstr "Normale kaart"

#: ../../reference_manual/filters/edge_detection.rst:69
msgid "Height to Normal Map"
msgstr "Hoogte tot normale kaart"

#: ../../reference_manual/filters/edge_detection.rst:76
msgid ""
"A filter that converts Height maps to Normal maps through the power of edge "
"detection. It is also available for the filter layer or filter brush."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:88
msgid "Channel"
msgstr "Kanaal"

#: ../../reference_manual/filters/edge_detection.rst:89
msgid ""
"Which channel of the layer should be interpreted as the grayscale heightmap."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:91
msgid ""
"The radius of the edge detection. Default is 1 and going higher will "
"increase the strength of the normal map. Adjust this if the effect of the "
"resulting normal map is too weak."
msgstr ""

#: ../../reference_manual/filters/edge_detection.rst:93
msgid ""
"An XYZ swizzle, that allows you to map Red, Green and Blue to different 3d "
"normal vector coordinates. This is necessary mostly for the difference "
"between MikkT-space normal maps (+X, +Y, +Z) and the OpenGL standard normal "
"map (+X, -Y, +Z)."
msgstr ""

#~ msgid "Sobol"
#~ msgstr "Sobol"
