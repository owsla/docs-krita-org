# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-07-12 11:43+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/filters/emboss.rst:1
msgid "Overview of the emboss filters."
msgstr "Overzicht van de reliëffilters."

#: ../../reference_manual/filters/emboss.rst:10
#: ../../reference_manual/filters/emboss.rst:15
msgid "Emboss"
msgstr "Reliëf"

#: ../../reference_manual/filters/emboss.rst:10
msgid "Filters"
msgstr "Filters"

#: ../../reference_manual/filters/emboss.rst:17
msgid ""
"Filters that are named by the traditional embossing technique. This filter "
"generates highlight and shadows to create an effect which makes the image "
"look like embossed. Emboss filters are usually used in the creation of "
"interesting GUI elements, and mostly used in combination with filter-layers "
"and masks."
msgstr ""

#: ../../reference_manual/filters/emboss.rst:20
msgid "Emboss Horizontal Only"
msgstr "Horizontaal reliëf"

#: ../../reference_manual/filters/emboss.rst:22
msgid "Only embosses horizontal lines."
msgstr "Alleen horizontale lijnen van reliëf voorzien."

#: ../../reference_manual/filters/emboss.rst:25
msgid "Emboss in all Directions"
msgstr "Reliëf in alle richtingen"

#: ../../reference_manual/filters/emboss.rst:27
msgid "Embosses in all possible directions."
msgstr "Van reliëf voorzien in alle mogelijke richtingen."

#: ../../reference_manual/filters/emboss.rst:30
msgid "Emboss (Laplacian)"
msgstr "Reliëf (Laplace-achtig)"

#: ../../reference_manual/filters/emboss.rst:32
msgid "Uses the laplacian algorithm to perform embossing."
msgstr "Het Laplace algoritme wordt gebruikt om van reliëf te voorzien."

#: ../../reference_manual/filters/emboss.rst:35
msgid "Emboss Vertical Only"
msgstr "Verticaal reliëf"

#: ../../reference_manual/filters/emboss.rst:37
msgid "Only embosses vertical lines."
msgstr "Alleen verticale lijnen in reliëf"

#: ../../reference_manual/filters/emboss.rst:40
msgid "Emboss with Variable depth"
msgstr "Reliëf met variabele diepte"

#: ../../reference_manual/filters/emboss.rst:42
msgid ""
"Embosses with a depth that can be set through the dialog box shown below."
msgstr ""

#: ../../reference_manual/filters/emboss.rst:45
msgid ".. image:: images/filters/Emboss-variable-depth.png"
msgstr ""

#: ../../reference_manual/filters/emboss.rst:47
msgid "Emboss Horizontal and Vertical"
msgstr ""

#: ../../reference_manual/filters/emboss.rst:49
msgid "Only embosses horizontal and vertical lines."
msgstr ""
