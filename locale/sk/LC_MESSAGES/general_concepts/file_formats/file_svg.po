# translation of docs_krita_org_general_concepts___file_formats___file_svg.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_svgm\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-03-01 14:15+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../general_concepts/file_formats/file_svg.rst:1
msgid "The Scalable Vector Graphics file format in Krita."
msgstr "Súborový formát Scalable Vector Graphics v Krita."

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "SVG"
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:10
#, fuzzy
#| msgid "\\*.svg"
msgid "*.svg"
msgstr "\\*.svg"

#: ../../general_concepts/file_formats/file_svg.rst:10
#, fuzzy
#| msgid "The Scalable Vector Graphics file format in Krita."
msgid "Scalable Vector Graphics Format"
msgstr "Súborový formát Scalable Vector Graphics v Krita."

#: ../../general_concepts/file_formats/file_svg.rst:15
msgid "\\*.svg"
msgstr "\\*.svg"

#: ../../general_concepts/file_formats/file_svg.rst:17
msgid ""
"``.svg``, or Scalable Vector Graphics, is the most modern vector graphics "
"interchange file format out there."
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:19
msgid ""
"Being vector graphics, SVG is very light weight. This is because it usually "
"only stores coordinates and parameters for the maths involved with vector "
"graphics."
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:21
msgid ""
"It is maintained by the W3C SVG working group, who also maintain other open "
"standards that make up our modern internet."
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:23
msgid ""
"While you can open up SVG files with any text-editor to edit them, it is "
"best to use a vector program like Inkscape. Krita 2.9 to 3.3 supports "
"importing SVG via the add shape docker. Since Krita 4.0, SVGs can be "
"properly imported, and you can export singlevector layers via :menuselection:"
"`Layer --> Import/Export --> Save Vector Layer as SVG...`. For 4.0, Krita "
"will also use SVG to save vector data into its :ref:`internal format "
"<file_kra>`."
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:25
msgid ""
"SVG is designed for the internet, though sadly, because vector graphics are "
"considered a bit obscure compared to raster graphics, not a lot of websites "
"accept them yet. Hosting them on your own webhost works just fine though."
msgstr ""
