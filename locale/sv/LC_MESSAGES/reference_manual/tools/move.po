# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:21+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Position"
msgstr "Position"

#: ../../<rst_epilog>:44
msgid ""
".. image:: images/icons/move_tool.svg\n"
"   :alt: toolmove"
msgstr ""
".. image:: images/icons/move_tool.svg\n"
"   :alt: Flyttningsverktyg"

#: ../../reference_manual/tools/move.rst:0
msgid ".. image:: images/tools/Movetool_coordinates.png"
msgstr ".. image:: images/tools/Movetool_coordinates.png"

#: ../../reference_manual/tools/move.rst:1
msgid "Krita's move tool reference."
msgstr "Referens för Kritas flyttningsverktyg."

#: ../../reference_manual/tools/move.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/move.rst:11
msgid "Move"
msgstr "Flytta"

#: ../../reference_manual/tools/move.rst:11
msgid "Transform"
msgstr "Transformera"

#: ../../reference_manual/tools/move.rst:16
msgid "Move Tool"
msgstr "Flyttningsverktyg"

#: ../../reference_manual/tools/move.rst:18
msgid "|toolmove|"
msgstr "|toolmove|"

#: ../../reference_manual/tools/move.rst:20
msgid ""
"With this tool, you can move the current layer or selection by dragging the "
"mouse."
msgstr ""
"Med det här verktyget kan man flytta nuvarande lager eller markering genom "
"att dra med musen."

#: ../../reference_manual/tools/move.rst:22
msgid "Move current layer"
msgstr "Flytta aktuellt lager"

#: ../../reference_manual/tools/move.rst:23
msgid "Anything that is on the selected layer will be moved."
msgstr "Allting som finns på det markerade lagret kommer att flyttas."

#: ../../reference_manual/tools/move.rst:24
msgid "Move layer with content"
msgstr "Flytta lager med innehåll"

#: ../../reference_manual/tools/move.rst:25
msgid ""
"Any content contained on the layer that is resting under the four-headed "
"Move cursor will be moved."
msgstr ""
"Allt innehåll som finns på lagret som ligger under den fyrhövdade "
"flyttmarkören kommer att flyttas."

#: ../../reference_manual/tools/move.rst:26
msgid "Move the whole group"
msgstr "Flytta hela gruppen"

#: ../../reference_manual/tools/move.rst:27
msgid ""
"All content on all layers will move.  Depending on the number of layers this "
"might result in slow and, sometimes, jerky movements. Use this option "
"sparingly or only when necessary."
msgstr ""
"Allt innehåll på alla lager kommer att flyttas. Beroende på antal lager kan "
"det orsaka långsamma och ibland ryckiga rörelser. Använd alternativet "
"sparsamt eller bara om nödvändigt."

#: ../../reference_manual/tools/move.rst:28
msgid "Shortcut move distance (3.0+)"
msgstr "Flyttningsavstånd för genväg (3.0+)"

#: ../../reference_manual/tools/move.rst:29
msgid ""
"This allows you to set how much, and in which units, the :kbd:`Left Arrow`, :"
"kbd:`Up Arrow`, :kbd:`Right Arrow` and :kbd:`Down Arrow` cursor key actions "
"will move the layer."
msgstr ""
"Låter dig ställa in och hur mycket, och med vilken enhet, som tangenterna :"
"kbd:`vänsterpil`, :kbd:`uppåtpil`, :kbd:`högerpil` and :kbd:`neråtpil` "
"flyttar lagret."

#: ../../reference_manual/tools/move.rst:30
msgid "Large Move Scale (3.0+)"
msgstr "Skalning för stora förflyttningar (3.0+)"

#: ../../reference_manual/tools/move.rst:31
msgid ""
"Allows you to multiply the movement of the Shortcut Move Distance when "
"pressing the :kbd:`Shift` key before pressing a direction key."
msgstr ""
"Låter dig multiplicera förflyttningen av Flyttningsavstånd för genväg när "
"tangenten :kbd:`Skift`hålls nere innan en riktningstangent används."

#: ../../reference_manual/tools/move.rst:32
msgid "Show coordinates"
msgstr "Visa koordinater"

#: ../../reference_manual/tools/move.rst:33
msgid ""
"When toggled will show the coordinates of the top-left pixel of the moved "
"layer in a floating window."
msgstr ""
"När aktiverad visas koordinaterna för bildpunkten längst upp till vänster av "
"lagret som flyttas i ett separat fönster."

#: ../../reference_manual/tools/move.rst:35
msgid ""
"If you click, then press the :kbd:`Shift` key, then move the layer, movement "
"is constrained to the horizontal and vertical directions. If you press the :"
"kbd:`Shift` key, then click, then move, all layers will be moved, with the "
"movement constrained to the horizontal and vertical directions"
msgstr ""
"Om man klickar och därefter håller nere tangenten :kbd:`Skift`, och sedan "
"flyttar lagret, är förflyttningen begränsad till den horisontella och "
"vertikala riktningarna. Om man håller nere tangenten :kbd:`Skift, därefter "
"klickar, och sedan flyttar, kommer alla lager att flyttas med förflyttningen "
"begränsad till den horisontella och vertikala riktningarna."

#: ../../reference_manual/tools/move.rst:37
msgid "Constrained movement"
msgstr "Begränsad förflyttning"

#: ../../reference_manual/tools/move.rst:40
msgid ""
"Gives the top-left coordinate of the layer, can also be manually edited."
msgstr ""
"Ger lagrets koordinat längst upp till vänster, kan också redigeras manuellt"
