# Translation of docs_krita_org_reference_manual___layers_and_masks___split_alpha.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___layers_and_masks___split_alpha\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:47+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:1
msgid ""
"Split Alpha: how to work with color and alpha channels of the layer "
"separately"
msgstr ""
"Відділення каналу прозорості: як працювати окремо із каналами кольорів і "
"каналом прозорості шару"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:11
msgid "Layers"
msgstr "Шари"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:11
msgid "Transparency"
msgstr "Прозорість"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:11
msgid "Alpha channel"
msgstr "Канал прозорості"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:11
msgid "Game"
msgstr "Гра"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:16
msgid "Split Alpha"
msgstr "Розділити прозорість"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:18
msgid ""
"Sometimes especially in the field of game development, artists need to work "
"with the alpha channel of the texture separately. To assist such workflow, "
"Krita has a special functionality called :menuselection:`Split Alpha`. It "
"allows splitting alpha channel of a paint layer into a separate :ref:"
"`transparency_masks`. The artist can work on the transparency mask in an "
"isolated environment and merge it back when he has finished working."
msgstr ""
"Іноді, особливо у галузі розробки ігор, художникам доводиться працювати над "
"каналом прозорості текстури (альфа-каналом) окремо. Щоб полегшити роботу у "
"цьому режимі, у Krita передбачено особливу можливість, яка має назву :"
"menuselection:`Розділити прозорість`. За її допомогою можна відділити альфа-"
"канал шару малювання у окрему :ref:`маску прозорості <transparency_masks>`. "
"Художник може працювати над маскою прозорості у ізольованому середовищі, а "
"потім об'єднати маску з рештою зображення, коли завершить роботу."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:21
msgid "How to work with alpha channel of the layer"
msgstr "Як працювати із каналом прозорості шару"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:23
msgid "|mouseright| the paint layer in the layers docker."
msgstr "Клацніть |mouseright| на пункті шару малювання на бічній панелі шарів."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:24
#: ../../reference_manual/layers_and_masks/split_alpha.rst:35
msgid "Choose :menuselection:`Split Alpha --> Alpha into Mask`."
msgstr "Виберіть :menuselection:`Розділити прозорість --> Прозорість у маску`."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:25
msgid ""
"Use your preferred paint tool to paint on the Transparency Mask. Black "
"paints transparency (see-through), white paints opacity (visible). Gray "
"values paint semi-transparency."
msgstr ""
"Скористайтеся вашим улюбленим інструментом для малювання для малювання на "
"масці прозорості. Чорним слід малювати прозорі ділянки, білим — непрозорі. "
"Проміжні сірі кольори визначатимуть напівпрозорі ділянки."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:26
msgid ""
"If you would like to isolate alpha channel, enter Isolated Mode by |"
"mouseright| + :menuselection:`Isolate Layer` (or the :kbd:`Alt +` |"
"mouseleft| shortcut)."
msgstr ""
"Якщо ви хочете ізолювати альфа-канал, увійдіть до ізольованого режиму за "
"допомогою клацання |mouseright| + :menuselection:`Ізолювати шар` (або "
"комбінації :kbd:`Alt` + |mouseleft|)."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:27
msgid ""
"When finished editing the Transparency Mask, |mouseright| on it and select :"
"menuselection:`Split Alpha --> Write as Alpha`."
msgstr ""
"Після завершення редагування маски прозорості клацніть на ній |mouseright| і "
"виберіть пункт :menuselection:`Розділити прозорість --> Записати як канал "
"прозорості`."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:30
msgid ""
"How to save a PNG texture and keep color values in fully transparent areas"
msgstr ""
"Як зберегти текстуру PNG і не змінити значення кольорів на повністю прозорих "
"ділянках"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:32
msgid ""
"Normally, when saving an image to a file, all fully transparent areas of the "
"image are filled with black color. It happens because when composing the "
"layers of the image, Krita drop color data of fully transparent pixels for "
"efficiency reason. To avoid this of color data loss you can either avoid "
"compositing of the image i.e. limit image to only one layer without any "
"masks or effects, or use the following method:"
msgstr ""
"Зазвичай, під час збереження зображення до файла, усі повністю прозорі "
"області зображення заповнюються чорним кольором. Так трапляється, оскільки "
"під час створення композиції із шарів зображення Krita відкидає дані "
"кольорів повністю прозорих пікселів із міркувань забезпечення ефективності "
"зберігання даних. Щоб уникнути цієї втрати даних кольорів, ви можете або "
"уникнути створення композиції зображення, тобто обмежити зображення лише "
"одним шаром без масок або ефектів, або скористатися таким методом:"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:34
msgid "|mouseright| the layer in the layers docker."
msgstr "Клацніть |mouseright| на пункті шару на бічній панелі шарів."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:36
msgid ""
"|mouseright| on the created mask and select :menuselection:`Split Alpha --> "
"Save Merged...`"
msgstr ""
"Клацніть |mouseright| на створеній масці і виберіть у меню пункт :"
"menuselection:`Розділити прозорість --> Зберегти об’єднане...`"
