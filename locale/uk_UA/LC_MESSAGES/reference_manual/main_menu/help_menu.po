# Translation of docs_krita_org_reference_manual___main_menu___help_menu.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___main_menu___help_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:34+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "About KDE"
msgstr "Про KDE"

#: ../../reference_manual/main_menu/help_menu.rst:1
msgid "The help menu in Krita."
msgstr "Меню «Довідка» у Krita."

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "About"
msgstr "Інформація"

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Handbook"
msgstr "Підручник"

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Bug"
msgstr "Вада"

#: ../../reference_manual/main_menu/help_menu.rst:16
msgid "Help Menu"
msgstr "Меню «Довідка»"

#: ../../reference_manual/main_menu/help_menu.rst:18
msgid "Krita Handbook"
msgstr "Підручник з Krita"

#: ../../reference_manual/main_menu/help_menu.rst:19
msgid "Opens a browser and sends you to the index of this manual."
msgstr "Відкриває браузер і переходить на сторінку покажчика цього підручника."

#: ../../reference_manual/main_menu/help_menu.rst:20
msgid "Report Bug"
msgstr "Надіслати звіт про помилку"

#: ../../reference_manual/main_menu/help_menu.rst:21
msgid "Sends you to the bugtracker."
msgstr "Відкриває сторінку системи звітування про вади."

#: ../../reference_manual/main_menu/help_menu.rst:22
msgid "Show system information for bug reports."
msgstr "Показати дані щодо системи для звітів щодо вад."

#: ../../reference_manual/main_menu/help_menu.rst:23
msgid ""
"This is a selection of all the difficult to figure out technical information "
"of your computer. This includes things like, which version of Krita you "
"have, which version your operating system is, and most prudently, what kind "
"of OpenGL functionality your computer is able to provide. The latter varies "
"a lot between computers and due that it is one of the most difficult things "
"to debug. Providing such information can help us figure out what is causing "
"a bug."
msgstr ""
"Тут буде зібрано усі технічні дані щодо вашого комп'ютера, які не є "
"очевидними. Серед іншого, тут будуть дані щодо версії Krita, якою ви "
"користуєтеся, версії операційної системи та, що є найцікавішим, типи "
"функціональних можливостей OpenGL, які надаються обладнанням та програмним "
"забезпеченням вашого комп'ютера. Цей набір даних може бути зовсім різним у "
"схожих комп'ютерів, отже, причину пов'язаних із ним помилок дуже важко "
"встановлювати. Надання вами даних щодо вашої системи може значно спростити "
"розробникам пошук причин вад у роботі програми."

#: ../../reference_manual/main_menu/help_menu.rst:24
msgid "About Krita"
msgstr "Про Krita"

#: ../../reference_manual/main_menu/help_menu.rst:25
msgid "Shows you the credits."
msgstr "Показує вікно із повідомленнями щодо авторів програми та ліцензування."

#: ../../reference_manual/main_menu/help_menu.rst:27
msgid "Tells you about the KDE community that Krita is part of."
msgstr ""
"Відкриває вікно із відомостями щодо спільноти KDE, частиною якої є Krita."
