msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/docs_krita_org_404.pot\n"

#: ../../404.rst:None
msgid ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Image of Kiki looking confused through books."
msgstr ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Kiki 困惑的看着书本的图像。"

#: ../../404.rst:5
msgid "File Not Found (404)"
msgstr "找不到文件 (404)"

#: ../../404.rst:10
msgid "This page does not exist."
msgstr "此页面不存在。"

#: ../../404.rst:12
msgid "This might be because of the following:"
msgstr "这可能是由于以下原因："

#: ../../404.rst:14
msgid ""
"We moved the manual from MediaWiki to Sphinx and with that came a lot of "
"reorganization."
msgstr ""
"我们把文档系统从 MediaWiki 换成了 Sphinx ，页面结构因此发生了重大变化。"

#: ../../404.rst:15
msgid "The page has been deprecated."
msgstr "该页面已废弃。"

#: ../../404.rst:16
msgid "Or a simple typo in the url."
msgstr "也可能只是 URL 存在拼写错误。"

#: ../../404.rst:18
msgid ""
"In all cases, you might be able to find the page you're looking for by using "
"the search in the lefthand navigation."
msgstr "不管是哪种情况，你都可以使用左侧导航栏找到所需页面。"
